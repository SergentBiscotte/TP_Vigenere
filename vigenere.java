public class vigenere {

    //Chiffrage du texte
    static String chiffrer_(String text, final String cle) 
    {
        String res = "";
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) 
        {
            char carac = text.charAt(i);
            //On s'assure que chaque caractère est situé entre A et Z et on continue le processus jusqu'à ne plus avoir de caractère à chiffrer
            if (carac < 'A' || carac > 'Z') 
            {
                continue;
            }

            res += (char)((carac + cle.charAt(j) - 2 * 'A') % 26 + 'A');
            j++;
            j = j % cle.length();
        }
        return res;
    }
 
    //Dechiffrage du texte
    static String dechiffrer(String text, final String cle) 
    {
        String res = "";
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) 
        {
            char carac = text.charAt(i);
            //On s'assure que chaque caractère est situé entre A et Z et on continue le processus jusqu'à ne plus avoir de caractère à dechiffrer
            if (carac < 'A' || carac > 'Z') 
            {
                continue;
            }
            
            res += (char)((carac - cle.charAt(j) + 26) % 26 + 'A');
            j++;
            j = j % cle.length();
        }
        return res;
    }
    public static void main(String[] args) 
    {
        //On modifie le contenu se trouvant entre guillemets dans la variable cle ou txt pour pouvoir faire fonctionner le chiffrage/déchiffrage comme on le souhaite
        String cle = "IUTAIXMARSEILLE";
        String txt = "CECIESTUNTEST";
        String chiffrer = chiffrer_(txt, cle);

        //Affichage du texte chiffrer ainsi que le texte originel ou dechiffrer
        System.out.println("Texte chiffrer : " + chiffrer);
        System.out.println("Texte originel/dechiffrer : " + dechiffrer(chiffrer, cle));
    }
}